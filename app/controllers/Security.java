package controllers;

import models.*;


public class Security extends Secure.Security{
    static boolean authenticate(String username, String password) {
        return Account.connect(username, password) != null;
    }

    static void onDisconnected() {
        Application.index();
    }

    static void onAuthenticated() {
        Admin.index();
    }

    static boolean check(String profile) {
        if("admin".equals(profile)) {
            return Account.find("byEmail", connected()).<Account>first().isAdmin;
        }
        return false;
    }

}
